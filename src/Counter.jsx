import React from "react";

function Counter({ item, handleIncrement, handleDecrement, handleDelete }) {
  return (
    <div className="item-container" id={item.id}>
      <button className="zero">{item.value}</button>
      <button onClick={() => handleIncrement(item.id)} className="increment">
        +
      </button>
      <button onClick={() => handleDecrement(item.id)} className="decrement">
        -
      </button>
      <button onClick={() => handleDelete(item.id)} className="delete">
        delete
      </button>
    </div>
  );
}

export default Counter;
