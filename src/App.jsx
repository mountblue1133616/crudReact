import React, { useState } from "react";
import "./App.css";
import Counter from "./Counter";

function App() {
  const defaultItems = [
    { id: 1, value: 0 },
    { id: 2, value: 0 },
    { id: 3, value: 0 },
    { id: 4, value: 0 },
  ];

  const [items, setItems] = useState(defaultItems);

  const handleIncrement = (itemId) => {
    setItems((prevItems) => {
      return prevItems.map((item) => {
        if (item.id === itemId) {
          return { ...item, value: item.value + 1 };
        }
        return item;
      });
    });
  };

  const handleDecrement = (itemId) => {
    setItems((prevItems) => {
      return prevItems.map((item) => {
        if (item.id === itemId) {
          if (item.value === 0) {
            return item;
          }
          return { ...item, value: item.value - 1 };
        }
        return item;
      });
    });
  };

  const handleDelete = (itemId) => {
    setItems((prevItems) => {
      return prevItems.filter((item) => item.id !== itemId);
    });
  };

  const handleReset = () => {
    if (items.length == 0) {
      setItems(defaultItems);
    }
  };

  const handleRefresh = () => {
    const refreshedItems = items.map((item) => {
      return { ...item, value: 0 };
    });
    setItems(refreshedItems);
  };
  return (
    <div className="container">
      <div className="cart-container">
        <h2>Cart</h2>
        <div className="items">
          {items.filter((item) => item.value > 0).length}
        </div>
        <h2>Items</h2>
      </div>
      <div className="reset-container">
        <button onClick={handleRefresh} className="refresh">
          Refresh
        </button>
        <button onClick={handleReset} className="reset">
          Reset
        </button>
      </div>
      {items.map((item) => (
        <Counter
          key={item.id}
          item={item}
          handleIncrement={handleIncrement}
          handleDecrement={handleDecrement}
          handleDelete={handleDelete}
        />
      ))}
    </div>
  );
}

export default App;
